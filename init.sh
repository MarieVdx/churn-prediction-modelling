sudo apt update
sudo apt-get install python3-venv

echo "***** Configuring directories *****"

mkdir data
cd data
mkdir models
mkdir uploads
mkdir results
cd ..


python3 -m venv .venv
echo ""
echo "*****  Virtual Environment created with success *****"
source .venv/bin/activate
echo ""
echo "*****  Virtual Environment activated *****"
echo ""
echo "***** Configuring web server *****"
pip install flask
export FLASK_APP=run.py
python -c 'import os; print(os.urandom(16))' > flask_secret
echo ""
echo "***** Installing required dependencies *****"
pip install pandas
pip install -U imbalanced-learn
pip install seaborn
pip install plotly==4.12.0
pip install scikit-optimize
pip install category-encoders
pip install eli5
pip install PDPbox
pip install pycebox
pip install shap
pip install jupyterlab
echo ""
echo "***** Congrats you're ready to go ! *****"
