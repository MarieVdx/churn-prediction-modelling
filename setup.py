from setuptools import setup, find_packages

setup(
    name="churn_predictor",
    url="https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-project/fall-2020/projet_ml_advanced_am_mv_rh/",
    author="Marie Verdoux, Rayann Hamrouni, Antoine Meilliez",
    description="AML project from Yotta Academy students",
    packages=find_packages()
)