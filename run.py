from churn_predictor.application.upload_app import app
from churn_predictor.settings import DEBUG

app.run(debug=DEBUG)