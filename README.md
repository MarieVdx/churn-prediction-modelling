# Project N°1: Advanced Machine Learning

_Subject 1: Churn Modeling_

Contributors: Antoine Meilliez, Marie Verdoux, Rayann Harmouni

## Usage
### 1 - Installation
You need a updated version of Python 3.8 to run this project. Check it with 
```
$ python3 --version
```
Process the following steps to install the project
```
# Clone the repository
$ git clone https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-project/fall-2020/projet_ml_advanced_am_mv_rh.git

# Move in the repository
$ cd PROJET_ML_ADVANCED_AM_MV_RH

# Initialize the project
$ source init.sh

# Your Are Ready !
```
Note: Remember to **always** activate your virtual environment before running the project. 

### 2 - Run the notebooks
1. Save your files `customers.csv` and `indicators.csv` in the `data` folder.
2. Run the following steps:
```
# Activate your virtual environment
$ source activate.sh

# Run jupyter
$ jupyter lab
```

### 3 - Run the web application
1. Run the following command:
```
# Run the web server
$ source run.sh
```
2. Go to http://127.0.0.1:5000/ 

### 3.1 - Model training
In order to train the model :
1. Run the web application as explained earlier
2. Add you files
3. Select "(Re)train the model"
4. Click on "Send your files"

> __Important Note__ : Prior to any use you have to train a first model with the inital data files `customers.csv` and `indicators.csv`.

### 3.2 - Prediction of new data
> __Important Note__ : You have to get trained a model at least once prior to perform prediction.
1. Run the web application as explained earlier
2. Add you files
3. Click on "Send your files"

A popup should appear inviting you to download your data enriched with the predictions.

### 4 - Project organisation

Code files are gathered in the directory `churn_predictor`.
Data used to run jupyter notebooks should be stored in the directory `data`.
your predictions results can be retieved in `data/results` folder as well as uploaded data files will be in `data/uploads`

```
PROJET_ML_ADVANCED_AM_MV_RH
├── churn_predictor
|    ├── application
|    |    ├── templates
|    |    ├── statics
|    ├── domain
|    ├── infrastructure
|    ├── interface
├── data
|    ├── models
|    ├── uploads
|    ├── results
├── notebooks
├── .gitignore
├── README.md
├── requirements.txt
├── setup.py
```


