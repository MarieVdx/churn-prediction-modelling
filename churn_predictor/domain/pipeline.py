"""
Module where classification pipelines are instanciated
"""
from imblearn.pipeline import Pipeline as imbPipeline
from joblib import dump, load
from pathlib import Path
from imblearn.under_sampling import RandomUnderSampler

from churn_predictor.domain.preprocessing import Base_Preprocessor 
from churn_predictor.domain.models import OPTIMIZED_MODEL
from churn_predictor.infrastructure.dataset import TrainingDataset
from churn_predictor.settings import CLF_DIR


#TODO : add existing classifiers path in Domain settings
CLF_PATH = Path(CLF_DIR)


PIPELINE_CONFIG = [('preprocessing', Base_Preprocessor),
                   ('Sampler', RandomUnderSampler(random_state=42)),
                   ('classifier', OPTIMIZED_MODEL)]


CHURN_PIPELINE = imbPipeline(steps = PIPELINE_CONFIG)


def save_pipeline(filename="churn_classifer.joblib"):
    path = CLF_PATH / filename
    dump(CHURN_PIPELINE, path)
    return path

def load_pipeline(filename= "churn_classifer.joblib"):
    path = CLF_PATH / filename
    pipeline = load(path)
    return pipeline

def existing_pipeline():
    test = (len(list(CLF_PATH.glob('**/*.joblib')))>0)
    print(test)
    return test