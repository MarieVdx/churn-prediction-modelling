import pandas as pd
from churn_predictor.settings import LABELS_COL, FEATURES, RESAMPLING



def undersampling(X_train, y_train):

    count_class_0, count_class_1, df_class_0, df_class_1 = prepare_sampling(X_train, y_train)


    # Undersampling
    df_class_0_under = df_class_0.sample(count_class_1, replace=True, random_state = 42)
    df_train_under = pd.concat([df_class_0_under, df_class_1], axis=0)


    X_train_under = df_train_under.drop(LABELS_COL, axis=1)
    y_train_under = df_train_under[LABELS_COL]

    return X_train_under, y_train_under


def oversampling(X_train, y_train):
    
    count_class_0, count_class_1, df_class_0, df_class_1 = prepare_sampling(X_train, y_train)

    # Oversampling
    df_class_1_over = df_class_1.sample(count_class_0, replace=True, random_state = 42)
    df_train_over = pd.concat([df_class_0, df_class_1_over], axis=0)


    # Rebuild training set
    X_train_over = df_train_over.drop(LABELS_COL, axis=1)
    y_train_over = df_train_over[LABELS_COL]


    return X_train_over, y_train_over

def prepare_sampling(X_train, y_train):

    df_train = pd.concat((pd.DataFrame(X_train, columns = FEATURES), 
                        pd.DataFrame(y_train, columns = [LABELS_COL])), axis = 1)


    # Class count
    count_class_0, count_class_1 = df_train[LABELS_COL].value_counts()

    # Divide by class
    df_class_0 = df_train[df_train[LABELS_COL] == 0]
    df_class_1 = df_train[df_train[LABELS_COL] == 1]

    return count_class_0, count_class_1, df_class_0, df_class_1

SAMPLING_FUNCS = {
    RESAMPLING[0]: undersampling,
    RESAMPLING[1]: oversampling
    }