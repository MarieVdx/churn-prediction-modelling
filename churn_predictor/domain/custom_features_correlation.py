import pandas as pd
import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt

def cramers_corrected_stat(conf_matrix):

    """ calculate Cramers V statistic for categorial-categorial association.
        uses correction from Bergsma and Wicher, 
        Journal of the Korean Statistical Society 42 (2013): 323-328
    """
    result=-1

    if conf_matrix.shape[0]==2:
        correct=False
    else:
        correct=True

    chi2 = stats.chi2_contingency(conf_matrix, correction=correct)[0]

    n = sum(conf_matrix.sum())
    phi2 = chi2/n
    r,k = conf_matrix.shape
    phi2corr = max(0, phi2 - ((k-1)*(r-1))/(n-1))    
    rcorr = r - ((r-1)**2)/(n-1)
    kcorr = k - ((k-1)**2)/(n-1)
    result=np.sqrt(phi2corr / min( (kcorr-1), (rcorr-1)))
    return result

def features_correlation(df, categorical_features = [], numeric_features = [], threshold = 0.05):
    """This function calculates the correlation and p_value between features in a dataframe.
    
    Depending on the types of the variables, the following statistics are used:
    - Spearman correlation and p_value if both feature and target variables are continuous
    - Cramers value (normalized Chi2) if both feature and target variables are categorical
    - Point biserial if one variable is continuous and the other is a binary categorial (2 categories)
    
    Note that the function does not cover the case for multicategorial variables and continuous variable.
    
     Parameters
    ----------
    df : Pandas dataframe 
    Contains the features and target values. Names of columns should correspond to numeric and categorical features.
    
    categorical_features : list
    List of dataframe column names containing the categorical features.
    
    numeric_features : list
    List of dataframe column names containing the numeric features.
    
    threshold : float in [0,1], feafault : 0.05
    Threshold to p_value for assessing the independance between feature and target.
    Value used to calculated the column 'Rejection of independance' of output_df.
   
       
    Returns
    -------
    corr : Pandas dataframe of floats in [-1, 1]
    Squared-table with as many index and columns as features providing a float value between -1 and 1 corresponding to  the correlation calculated with the statistics described above based on the features types.
    
    p_value : Pandas dataframe of floats in [0, 1] 
    Squared-table with as many index and columns as features providing a float value between 0 and 1 providing the p_value corresponding to the correlation statistics.
    
    Rejection of independance: Pandas dataframe of bool
    SSquared-table with as many index and columns as features assessing the independance of the features.
    
    If p_value <= threshold : the null hypothesis of independance can be rejected. 
    If p_value > threshold : the null hypothesis of independance cannot be rejected. """
    
    
    features = categorical_features + numeric_features
    corr = pd.DataFrame(index = features, columns = features)
    p_val = pd.DataFrame(index = features, columns = features)
    
    for f1 in features:
        for f2 in features:
	
			
            if f1 == f2:
                corr[f1][f2] = 1
                p_val[f1][f2] = 0
            else: 
                
                # Deux variables catégorielles : statistique de Cramers et p_value par le test du Chi2
                if (f1 in categorical_features) & (f2 in categorical_features):
                    confusion_matrix = pd.crosstab(df[f1], df[f2])
                    corr[f1][f2]= cramers_corrected_stat(confusion_matrix)
                    p_val[f1][f2]= stats.chi2_contingency(confusion_matrix)[1]  
                
                # Deux variables numériques : corrélation de Spearmans
                elif (f1 in numeric_features) & (f2 in numeric_features):
                    corr[f1][f2]=stats.spearmanr(df[f1],df[f2])[0]
                    p_val[f1][f2]=stats.spearmanr(df[f1],df[f2])[1]
                     
                # Pour une variable continue et une variable catégorielle: point biserial                                    
                elif (f1 in numeric_features) & (f2 in categorical_features):
                    f2_values = df[f2].unique()
                    if len(f2_values) == 2:
                        f2_bool = df[f2].replace({f2_values[0]: 0, f2_values[1]: 1})
                        corr[f1][f2] = stats.pointbiserialr(df[f1],f2_bool)[0]
                        p_val[f1][f2] = stats.pointbiserialr(df[f1],f2_bool)[1]
                        
                elif (f1 in categorical_features) & (f2 in numeric_features):
                    f1_values = df[f1].unique()
                    if len(f1_values) == 2:
                        f1_bool = df[f1].replace({f1_values[0]: 0, f1_values[1]: 1})
                        corr[f1][f2] = stats.pointbiserialr(f1_bool, df[f2])[0]
                        p_val[f1][f2] = stats.pointbiserialr(f1_bool, df[f2])[1]

    corr = corr.astype('float32')
    p_val = p_val.astype('float32')
    
    df_reject_ind = p_val<0.05
                                  
    
    return corr, p_val, df_reject_ind


def correlation_features_to_target(df, target, categorical_features = [], numeric_features = [], is_target_categorical = True, threshold = 0.05):
    """This function calculates the correlation value and the value of categorical and numeric features with a target column.
    
    Depending on the types of the variables, the following statistics are used:
    - Spearman correlation and p_value if both feature and target variables are continuous
    - Cramers value (normalized Chi2) if both feature and target variables are categorical
    - Point biserial if one variable is continuous and the other is a binary categorial (2 categories)
    
    Note that the function does not cover the case for multicategorial variables.
    
     Parameters
    ----------
    df : Pandas dataframe 
    Contains the features and target values. Names of columns should correspond to numeric, categorical features and target.
       
    target : str
    Name of dataframe column containing the target values
    
    categorical_features : list
    List of dataframe column names containing the categorical features.
    
    numeric_features : list
    List of dataframe column names containing the numeric features.

    is_target_categorical : bool
    True if the target columns is categorical (2 values only). False is target values are continuous.
    
    threshold : float in [0,1], feafault : 0.05
    Threshold to p_value for assessing the independance between feature and target.
    Value used to calculated the column 'Rejection of independance' of output_df.
   
       
    Returns
    -------
    output_df : Pandas dataframe 
    Table with 3 columns and as many index as features providing for each feature:
    
    - 'Correlation' : float, the correlation between feature and target calculated with the statistics described above based on the features and target types.
    
    - 'p_value' : float, p_value corresponding to the correlation statistics.
    
    - 'Rejection of independance': bool, assessing the independance of the features with the target.
    If p_value <= threshold : the null hypothesis of independance can be rejected. The corresponding feature should be kept for the model.
    If p_value > threshold : the null hypothesis of independance cannot be rejected. The corresponding feature can probably be dropped for the model."""
    
    features = categorical_features + numeric_features
    output_df = pd.DataFrame(columns = features, index = ['Correlation', 'p_value'])
	
	
    for f in categorical_features:

        if is_target_categorical :
            confusion_matrix = pd.crosstab(df[f], df[target])
            output_df[f]['Correlation']= cramers_corrected_stat(confusion_matrix)
            output_df[f]['p_value']= stats.chi2_contingency(confusion_matrix)[1]  

        else:
            f_values = df[f].unique()
            if len(f_values) == 2:
                f_bool = df[f].replace({f_values[0]: 0, f_values[1]: 1})
                output_df[f]['Correlation'] = stats.pointbiserialr(target_bool, df[f])[0]
                output_df[f]['p_value'] = stats.pointbiserialr(target_bool, df[f])[1]

    for f in numeric_features :
			
        if is_target_categorical :
            target_values = df[target].unique()
            if len(target_values) == 2:
                target_bool = df[target].replace({target_values[0]: 0, target_values[1]: 1})
                output_df[f]['Correlation'] = stats.pointbiserialr(target_bool, df[f])[0]
                output_df[f]['p_value'] = stats.pointbiserialr(target_bool, df[f])[1]

        else:
            output_df[f]['Correlation']=stats.spearmanr(df[f],df[target])[0]
            output_df[f]['Correlation']=stats.spearmanr(df[f],df[target])[1]
    
    output_df = output_df.transpose()

    output_df['Rejection of independance'] = output_df['p_value'] < threshold
    
    output_df = output_df.sort_values(by='p_value', ascending=1)

    return output_df

