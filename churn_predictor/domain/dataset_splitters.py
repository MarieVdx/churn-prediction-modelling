from sklearn.model_selection import train_test_split
from churn_predictor.domain.resampling import SAMPLING_FUNCS


class RandomSplitter:
    """Split Datasetsinto random train and test subsets
    """
    def __init__(self, training_dataset, test_size=0.33, random_state=42, resampling=None):

        self.features_train, self.features_test, self.labels_train, self.labels_test = train_test_split(
            training_dataset.features,
            training_dataset.labels,
            test_size=test_size, 
            random_state=random_state)
        if resampling is not None:
            self.features_train, self.labels_train = SAMPLING_FUNCS[resampling](
                self.features_train, self.labels_train)

        
    @property
    def testing_sets(self):
        return self.features_test, self.labels_test
    
    @property
    def training_sets(self):
        return self.features_train, self.labels_train,
    
    @property
    def X_train(self):
        return self.features_train

    @property
    def X_test(self):
        return self.features_test

    @property
    def y_train(self):
        return self.labels_train

    @property
    def y_test(self):
        return self.labels_test
