import numpy as np

from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.metrics import roc_curve, precision_recall_curve, average_precision_score, roc_auc_score, recall_score, precision_score, confusion_matrix

from churn_predictor.domain.churn_scorer import churn_loss_function, worst_cases_costs
from churn_predictor.domain.resampling import SAMPLING_FUNCS
from churn_predictor.settings import RESAMPLING, BASE_NAME, SCORES_NAMES, REVERSED_RANK_SCORES
from churn_predictor.domain.preprocessing import Base_Preprocessor




class PipelineBenchmark:
    """ 
        This Class package some utils enabling sklearn pipeline benchmark. This class can be used to perform model updates. 
        It is admited that baseline should be this up-to-date model in order to compare new results with relevant performance. 

        Parameters
        --------        
        X, y ; arrays
            Features and Labels to use for the benchmark.
        
        test_size: float (optional)
            Ratio of split between the train and the test dataset. Must be between 0 and 1.


        Attributes
        --------
        X_train, X_test, y_train, y_test: arrays
            Data to use for each evaluation
        
        scores: dict
            The scores dictionnary has for key the pipeline name (`str`) and for value a `numpy.array` of two columns. 
            The first column contains the performances scores
            The second contains the percentage of evolution compare to the baseline.
            Evaluated scores are included in the `SCORES_NAMES` config variable.

    """
    
    def __init__(self, X, y, test_size = .33):
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=test_size, random_state=42)
        self.scores = {}
        self.evaluate(self.base_pipeline, BASE_NAME, init=True)
    
    def print_scores_of(self, name):
        scores = self.scores[name][0]
        print(f"Scores of {name}")
        for i, score in enumerate(scores):
            print(f" - {SCORES_NAMES[i]}: {score}")

    def savings_allowed_by(self, name):
        active_cost, passive_cost = worst_cases_costs()
        model_cost = self.scores[name][0][0]
        saving_vs_active = active_cost - model_cost
        saving_vs_passive = passive_cost - model_cost
        return saving_vs_active, saving_vs_passive


    def evaluate(self, clf, clf_name, init=False, resampling=None):

        if resampling is None:
            X_train, y_train = self.X_train, self.y_train
        elif resampling in RESAMPLING:
            X_train, y_train = self.resample(resampling)
        else:
            raise ValueError(f"Invalid value of resampling. Please use one from setting.RESAMPLING: {RESAMPLING}")
            
        #Prepare the model
        clf.fit(self.X_train, self.y_train)

        #Perform test
        y_pred = clf.predict(self.X_test)
        if hasattr(clf, "decision_function"):
            y_score = clf.decision_function(self.X_test)
        else:
            y_score = clf.predict_proba(self.X_test)[:, 1]
        
        #Compute metrics curves
        fpr_roc, tpr_roc, thresholds = roc_curve(self.y_test, y_score)
        precision, recall, thresholds = precision_recall_curve(self.y_test, y_score)
        conf_matrix = confusion_matrix(self.y_test, y_pred)

        #Compute numeric metrics
        scores = [
            churn_loss_function(self.y_test, y_pred),
            average_precision_score(self.y_test,y_score),
            roc_auc_score(self.y_test,y_score),
            recall_score(self.y_test,y_pred),
            precision_score(self.y_test,y_pred),
        ]
        scores = np.around(scores, decimals=2)

        if not init:
            diff_baseline = np.round((scores - self.scores[BASE_NAME][0])/self.scores[BASE_NAME][0],3)*100
        else:
            diff_baseline = np.zeros(len(scores))

        self.scores[clf_name] = np.array([scores, diff_baseline])

        return fpr_roc, tpr_roc, precision, recall, self.scores[clf_name], conf_matrix

    @property
    def base_pipeline(self):
        #TODO : change it with a call the package pipeline.
        return Pipeline(steps=[('preprocessor', self.base_transformer),
                      ('classifier', self.base_classifier)])
    
    @property
    def base_transformer(self):
        #TODO: remove it
        numeric_features = ['AGE', 'BALANCE', 'NB_PRODUITS', "SCORE_CREDIT", ]
        numeric_transformer = Pipeline(steps=[
            ('imputer', SimpleImputer(strategy='median')),
            ('scaler', StandardScaler())])

        categorical_features = ['PAYS', 'SEXE', "MEMBRE_ACTIF"]
        categorical_transformer = Pipeline(steps=[
            ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
            ('onehot', OneHotEncoder(handle_unknown='ignore'))])

        preprocessor = ColumnTransformer(
            transformers=[
                ('num', numeric_transformer, numeric_features),
                ('cat', categorical_transformer, categorical_features)])
        return Base_Preprocessor
    
    @property
    def base_classifier(self):
        return SVC()
    
    @property
    def palmares(self, top = 5):
        for i , score_name in enumerate(SCORES_NAMES):
            print("-"*80)
            print(f'Palmares (top {top}): {score_name}')
            ranking = sorted(self.scores.items(), key=lambda x: x[1][0][i], reverse=(score_name in REVERSED_RANK_SCORES))
            for rank, record in enumerate(ranking):
                rank +=1
                if rank <= top:
                    print(f"   {rank} - {record[0]}: {record[1][0][i]}")

    def resample(self, resampling):
        X_train, y_train = SAMPLING_FUNCS[resampling](self.X_train, self.y_train)
        return X_train, y_train


    
