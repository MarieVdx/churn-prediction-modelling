"""
Module contains the models used to predict churn 
"""

from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier


SVC = svm.SVC()
BASE_EST = DecisionTreeClassifier(max_depth=3)
OPTIMIZED_MODEL = AdaBoostClassifier(
        base_estimator= BASE_EST,
        algorithm='SAMME',
        n_estimators= 1000,
        learning_rate= 0.1,
    )

