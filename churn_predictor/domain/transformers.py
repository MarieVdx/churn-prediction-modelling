"""
Module to create preprocessing transformers. 
These transformers aims to be used in the classification pipeline.

Classes
--------
ChurnColumnTransformer

"""

import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import FunctionTransformer
from churn_predictor.settings import SELECTED_FEATURES


BASE_MODEL_NUMERIC_TRANSFORMER = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='median')),
    ('scaler', StandardScaler())])

BASE_MODEL_CATEGORICAL_TRANSFORMER = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
    ('onehot', OneHotEncoder(handle_unknown='ignore'))])


class SelectiveNaNRemover(BaseEstimator, TransformerMixin):
    """ Transformer removing observations having NaNs in specific columns
    """
    def __init__(self, features_to_remove_nan_values_from):
        if not isinstance(features_to_remove_nan_values_from, list):
            features_to_remove_nan_values_from = [features_to_remove_nan_values_from]
        self.features = features_to_remove_nan_values_from

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        assert isinstance(X, pd.DataFrame)
        return X.dropna(subset = self.features)

class FeatureSelector(BaseEstimator, TransformerMixin):
    """ Transformer selecting features based on a selected feature list
    """
    def __init__(self, features):
        self.features = features

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        assert isinstance(X, pd.DataFrame)
        return X[self.features]


class CustomDiscretizer(BaseEstimator, TransformerMixin):
    """ Transformer discretizing continous features with custom bins
    """
    def __init__(self, bins, right=True, retbins=False,
                 precision=3, include_lowest=True,
                 duplicates='raise'):
        self.bins = bins
        self.right = right
        self.labels = False
        self.retbins = retbins
        self.precision = precision
        self.include_lowest = include_lowest
        self.duplicates = duplicates

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        if not isinstance(X, pd.DataFrame):
            try:
                X = pd.DataFrame(X, columns = SELECTED_FEATURES)
            except Exception as e:
                print("Fail to convert X in DataFrame during CustomDiscretizer")
                raise e
        for jj in range(X.shape[1]):
            X.iloc[:, jj] = pd.cut(x=X.iloc[:, jj].values, **self.__dict__)
        return X


class FeaturesDiscretizer(ColumnTransformer):
    """ ColumnTransformer for discretizing multiple columns
    """
    def __init__(self):
        super().__init__(transformers=[
            ('age_discretizer', CustomDiscretizer(bins = AGE_BINS), AGE_COL),
            ('balance_discretizer', CustomDiscretizer(bins = BALANCE_BINS), BALANCE_COL),
            ('score_credit_discretizer', CustomDiscretizer(bins = SCORE_BINS), SCORE_COL)
            ])

