from sklearn.pipeline import Pipeline

from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder

from churn_predictor.domain.transformers import CustomDiscretizer, SelectiveNaNRemover, FeatureSelector, BASE_MODEL_CATEGORICAL_TRANSFORMER,BASE_MODEL_NUMERIC_TRANSFORMER
from churn_predictor.settings import AGE_BINS, AGE_COL, BALANCE_BINS, BALANCE_COL, SCORE_COL, SCORE_BINS, REVENUE_COL, SELECTED_FEATURES, BASE_MODEL_CATEGORICAL_FEATURES,BASE_MODEL_NUMERIC_FEATURES


Discretizer = ColumnTransformer(
    [('age_discretizer', CustomDiscretizer(bins = AGE_BINS), AGE_COL),
    ('balance_discretizer', CustomDiscretizer(bins = BALANCE_BINS), BALANCE_COL),
    ('score_credit_discretizer', CustomDiscretizer(bins = SCORE_BINS), SCORE_COL)
    ], remainder='passthrough')


Preprocessor = Pipeline(steps = [
    ('cleaner', SelectiveNaNRemover(features_to_remove_nan_values_from = REVENUE_COL)),
    ('features_selector', FeatureSelector(features = SELECTED_FEATURES)),
    ('discretizer', Discretizer),
    ('imputer', SimpleImputer(strategy = 'most_frequent')),
    ('encoder', OneHotEncoder(handle_unknown='ignore')),
    ])


Base_Preprocessor = ColumnTransformer(
                transformers=[
                    ('num', BASE_MODEL_NUMERIC_TRANSFORMER, BASE_MODEL_NUMERIC_FEATURES),
                    ('cat', BASE_MODEL_CATEGORICAL_TRANSFORMER, BASE_MODEL_CATEGORICAL_FEATURES)
                    ])

