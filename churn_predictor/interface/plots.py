import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import plotly.graph_objects as go
from sklearn.metrics import roc_curve, auc, precision_recall_curve, average_precision_score, confusion_matrix
from plotly.subplots import make_subplots
from churn_predictor.domain.churn_scorer import create_churn_cost_matrix
from churn_predictor.domain.model_benchmark import SCORES_NAMES
import plotly.figure_factory as ff

def plot_histograms_comparisons(df, hue='CHURN', multiple="stack"):
    """Plot histograms distributions of each columns. 
    Hue is defined by default at 'CHURN' but it can be any columns of the dataframe."""

    nrows = math.ceil((len(df.columns)-1)/3)
    ncols = 3
    fig, axs = plt.subplots(ncols=ncols, nrows=nrows, figsize=(8*ncols,5*nrows))
    for i, col_name in enumerate(df.columns):
        if col_name != hue:
            row, col = int(i/3), int(i%3)
            sns.histplot(df, x = col_name, hue=hue, multiple=multiple, ax = axs[row][col])
    return fig

def plot_boxplot_comparisons(df, hue="CHURN"):
    """ Plot catégorical/numerical comparisons for the given dataframe
    """
    pal = sns.diverging_palette(220, 20, n=3)
    figsize = (6*len(categorical_features), 5*len(numeric_features))
    fig, axs = plt.subplots(nrows=len(categorical_features), ncols=len(numeric_features), figsize=figsize)
    for r, cat in enumerate(categorical_features):
        for c , num in enumerate(numeric_features):
            sns.boxplot(x=cat, y=num, hue=hue, data=df, ax=axs[r][c], width=0.5, palette=pal)
        
def plot_violinplot_comparisons(df, hue="CHURN"):
    """ Plot distribution comparisons for each numerical feature regarding categorical features.
    """
    figsize = (6*len(categorical_features), 5*len(numeric_features))
    fig, axs = plt.subplots(nrows=len(categorical_features), ncols=len(numeric_features), figsize=figsize)
    for r, cat in enumerate(categorical_features):
        for c , num in enumerate(numeric_features):
            sns.violinplot(x=cat, y=num, hue=hue, data=df, ax=axs[r][c], split=True,scale="count", inner="points", palette="Set2" )

def plot_roc_pr_report(y_test, y_score):
    """ Plot ROC and Precision recal curve
    
    Parameters
    -----------
    y_test: numpy.array
        True values of labels from the test sample
    y_score: numpy.array
        Score values from the fitted classifiers.
        For SVC scores can be optained with: clf.decision_function(X_test)

    """

    fpr_roc, tpr_roc, thresholds = roc_curve(y_test, y_score)
    precision, recall, thresholds = precision_recall_curve(y_test, y_score)
    average_precision = average_precision_score(y_test, y_score)

    fig = make_subplots(rows=1, cols=2, subplot_titles=(
        f'ROC Curve (AUC={round(auc(fpr_roc, tpr_roc),2)})',
        f'Precision-Recall Curve (AP={round(average_precision,2)})'),
                       shared_xaxes=True, shared_yaxes=True)

    row = 1
    # ROC subplot
    col = 1
    fig.add_trace(go.Scatter(x=fpr_roc, y=tpr_roc,fill='tozeroy', name="ROC"), row=row, col=col,
    )
    fig.update_xaxes(title_text="False Positive Rate", row=row, col=col, constrain='domain')
    fig.update_yaxes(title_text="True Positive Rate", row=row, col=col, scaleanchor="x", scaleratio=1)
    fig.add_shape(
        type='line', line=dict(dash='dash'),
        x0=0, x1=1, y0=0, y1=1,row=row, col=col,
    )

    # Precision-Recall subplot
    label_counts = y_test.value_counts().to_dict()
    positives_ratio = round(label_counts[1]/label_counts[0],2)
    col=2
    fig.add_trace(go.Scatter(x=recall, y=precision,fill='tozeroy', name="Precision-Recall"), row=row, col=col,
    )
    fig.update_xaxes(title_text="Recall", row=row, col=col, constrain='domain')
    fig.update_yaxes(title_text="Précision", row=row, col=col, scaleanchor="x", scaleratio=1)
    fig.add_shape(
        type='line', line=dict(dash='dash'),
        x0=0, x1=1, y0=positives_ratio, y1=positives_ratio, row=row, col=col,
    )
    
    return fig

def plot_churn_cost_matrix(client_acquisition_cost, churn_action_cost, churn_action_efficiency, churn_rate):
    """ Plot simulation of the churn prevention action cost regarding different levels of modèle performance.
    Draws a rectangle arount the profitables cases 
    
    Parameters
    --------
    client_acquisition_cost: Int
        Sales and marketing cost relatited to the acquisition of a new customer.
    churn_action_cost: Int
        Marketing and customer service cost related to the customer churn prevention.
    churn_action_efficiency: Int
        Proportion of people leaving the bank despite the prevention action. Value must be between 1 and 100.
    churn_rate: Int
        Proportion of churning people in the dataset. Value must be between 1 and 100.

    """

    cost_mat, labels = create_churn_cost_matrix(client_acquisition_cost, churn_action_cost, churn_action_efficiency, churn_rate)
    call_everybody_cost = cost_mat[-1, 0]

    indexes_of_elements_to_outline = zip(*np.where(cost_mat<call_everybody_cost))

    with sns.axes_style("white"):
        fig, ax = plt.subplots(1, 1, figsize=(15,15))
        ax = sns.heatmap(cost_mat.T, xticklabels=labels, yticklabels=labels, square=True,annot=True,linewidths=.5, cbar_kws={'label':'Cost per Consumer (€)'})
        ax.invert_yaxis()

        for index in indexes_of_elements_to_outline:
            rect = plt.Rectangle(index, 1,1, color="blue", 
                                linewidth=3, fill=False, clip_on=False)
            ax.add_patch(rect)

    plt.title("Unit cost of errors")
    plt.xlabel("Sensitivy - Good at predicting CHURN [1] (%)")
    plt.ylabel(" Specificity - Good at predicting NOT CHURN [0] (%)")
    ax.grid(True)
    plt.show()



def plot_benchmark(y_test, name, fpr_roc, tpr_roc, precision, recall, scores, conf_matrix):
    """
    fpr_roc, tpr_roc: arrays
        Values resulting from the call of `roc_curve` sklearn method

    precision, recall: arrays
        Values resulting from the call of `precision_recall_curve` sklearn method
    
    scores: list
        list of scores values to be displayed in the table
    """
    fig = make_subplots(rows=1, cols=4,
                        shared_xaxes=True, shared_yaxes=True, 
                        subplot_titles=("ROC Curve", "Precision-Recall", "Confusion Matrix (x:Preds, y:True)", "Scoring table"),
                        specs=[[{"type": "scatter"},{"type": "scatter"},{"type":"heatmap"},{"type": "table"}]]
                        )
    
    row = 1
    try:
        #ROC Curve
        col = 1
        fig.add_trace(
            go.Scatter(x=fpr_roc, y=tpr_roc,fill='tozeroy', name="ROC"), 
            row=row, col=col,
        )
        fig.update_xaxes(title_text="False Positive Rate", row=row, col=col, constrain='domain')
        fig.update_yaxes(title_text="True Positive Rate", row=row, col=col, scaleanchor="x", scaleratio=1)
        fig.add_shape(
            type='line', line=dict(dash='dash'),
            x0=0, x1=1, y0=0, y1=1,row=row, col=col,
        )

        # Precision-Recall subplot
        label_counts = y_test.value_counts().to_dict()
        positives_ratio = round(label_counts[1]/label_counts[0],2)
    except Exception as e:
        print("An error occured during ROC Curve ploting")
        print(repr(e))
    try:    
        col=2
        fig.add_trace(
            go.Scatter(x=recall, y=precision,fill='tozeroy', name="Precision-Recall"), 
            row=row, col=col,
        )
        fig.update_xaxes(title_text="Recall", row=row, col=col, constrain='domain')
        fig.update_yaxes(title_text="Précision", row=row, col=col, scaleanchor="x", scaleratio=1)
        fig.add_shape(
            type='line', line=dict(dash='dash'),
            x0=0, x1=1, y0=positives_ratio, y1=positives_ratio, row=row, col=col,
        )
    except Exception as e:
        print("An error occured during Precision Recall Curve ploting")
        print(repr(e))
    try:
        #Confusion matrix
        col=3
        fig_map = ff.create_annotated_heatmap(z=conf_matrix, x=["No Churn", "Churn"], y=["No Churn", "Churn"], 
            annotation_text=conf_matrix, colorscale='RdBu')
        annot = list(fig_map.layout.annotations)
        fig.add_trace(fig_map.data[0],
            row=row, col=col,
        )
        for k in range(len(annot)):
            annot[k]['xref'] = 'x3'
            annot[k]['yref'] = 'y3'
        for anno in annot:
            fig.add_annotation(anno)

        """fig.add_trace(
            go.Heatmap(z=conf_matrix,
            x=['No Churn', 'Churn'],
            y=['No Churn', 'Churn']),
            row=row, col=col
        )"""
    except Exception as e:
        print("An error occured during the confusion matrix ploting")
        print(repr(e))    
    
    try:
        # Performance table
        col=4
        table_values = [SCORES_NAMES,*scores]
        fig.add_trace(
            go.Table(
                header=dict(
                    values=["Metric", "Value", "Evolution (%)"],
                    font=dict(size=10),
                    align="left"
                ),
                cells=dict(
                    values=table_values,
                    align = "left")),
        row=row, col=col
        )
    except Exception as e:
        print("An error occured during the scoring table display")
        print(repr(e))
    
    fig.update_layout(showlegend=False,
                 autosize=False,
                 height = 400,
                 width = 1200,
                 title_text = name,
                 )
    fig.show()