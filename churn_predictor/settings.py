"""
Contains all configurations for the projectself.
Should NOT contain any secrets.

>>> import settings
>>> settings.DATA_DIR
"""
import os
import logging


DEBUG = True
ACCEPTED_UPLOAD_EXTENTIONS = ['.csv']
try :
    with open('flask_secret') as f:
        SS_TK = f.read()
except:
    print("The file flask_secret can't be found. It is required if you aim to run Churn Predictor web application and not only the notebooks. If needed, please run init.sh")

# By default the data is stored in this repository's "data/" folder.
# You can change it in your own settings file.
REPO_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../'))
DATA_DIR = os.path.join(REPO_DIR, 'data')
CLF_DIR = os.path.join(DATA_DIR, 'models')
UPLOADS_DIR = os.path.join(DATA_DIR, 'uploads')
PREDS_DIR = os.path.join(DATA_DIR, 'results')
OUTPUTS_DIR = os.path.join(REPO_DIR, 'outputs')
LOGS_DIR = os.path.join(REPO_DIR, 'logs')


# Logging
def enable_logging(log_filename, logging_level=logging.DEBUG):
    """Set loggings parameters.

    Parameters
    ----------
    log_filename: str
    logging_level: logging.level

    """
    with open(os.path.join(LOGS_DIR, log_filename), 'a') as file:
        file.write('\n')
        file.write('\n')

    LOGGING_FORMAT = '[%(asctime)s][%(levelname)s][%(module)s] - %(message)s'
    LOGGING_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

    logging.basicConfig(
        format=LOGGING_FORMAT,
        datefmt=LOGGING_DATE_FORMAT,
        level=logging_level,
        filename=os.path.join(LOGS_DIR, log_filename)
    )


ENTRY_DATE_COL = 'DATE_ENTREE'
ID_COL = 'ID_CLIENT'
NAME_COL = 'NOM'
COUNTRY_COL = 'PAYS'
GENDER_COL = 'SEXE'
ACTIVITY_COL = 'MEMBRE_ACTIF'

PRODUCT_COL = 'NB_PRODUITS'
CARD_COL = 'CARTE_CREDIT'
AGE_COL = 'AGE'
BALANCE_COL = 'BALANCE'
SCORE_COL = 'SCORE_CREDIT'
REVENUE_COL = 'SALAIRE'

LABELS_COL = 'CHURN'

FEATURES = [ENTRY_DATE_COL, 
            ID_COL,
            NAME_COL,
            COUNTRY_COL,
            GENDER_COL,
            ACTIVITY_COL,
            PRODUCT_COL,
            CARD_COL,
            AGE_COL,
            BALANCE_COL,
            SCORE_COL,
            REVENUE_COL
            ]


CATEGORICAL_FEATURES = [PRODUCT_COL,
                    COUNTRY_COL,
                    GENDER_COL,
                    ACTIVITY_COL,
                    ]

NUMERIC_FEATURES = [AGE_COL,
                    BALANCE_COL,
                    SCORE_COL]

SELECTED_FEATURES = CATEGORICAL_FEATURES + NUMERIC_FEATURES

BASE_MODEL_NUMERIC_FEATURES = [AGE_COL, BALANCE_COL, PRODUCT_COL, SCORE_COL, ]
BASE_MODEL_CATEGORICAL_FEATURES = [COUNTRY_COL, GENDER_COL, ACTIVITY_COL]

CUSTOMERS_COLS = [ID_COL,ENTRY_DATE_COL,NAME_COL,COUNTRY_COL, GENDER_COL, AGE_COL]
INDICATORS_COLS = [ID_COL, BALANCE_COL, PRODUCT_COL, CARD_COL, REVENUE_COL, SCORE_COL]

['ID_CLIENT', 'BALANCE', 'NB_PRODUITS', 'CARTE_CREDIT', 'SALAIRE',
    'SCORE_CREDIT']


AGE_BINS = [18, 25, 35, 40, 45, 50, 55, 60, 70, float("inf")]
BALANCE_BINS = [0, 0.1, 80000,  100000,  120000,  140000, 160000, float("inf")]
SCORE_BINS = [350, 400, 450, 550, 650, 750, 850]

MODEL_PARAM = {'C': 1,
               'kernel': 'rbf'
            }

RESAMPLING = ["undersampling", "oversampling"]

BASE_NAME = "Baseline classification"
SCORES_NAMES = ["Churn_Cost", "Av_Prec","ROC_AUC", "Recall","Precision"]
REVERSED_RANK_SCORES = SCORES_NAMES[1:]
