import os
from pathlib import Path
import pandas as pd
from datetime import datetime 

from churn_predictor.settings import FEATURES, ENTRY_DATE_COL, ID_COL, DATA_DIR, PREDS_DIR, REVENUE_COL, LABELS_COL

class Dataset:
    """
    Raw structure of dataset for training and testing 

    Parameters
    --------
    filepaths: str or list<str>
        Path of list of path where datasets can be found
    
    Properties
    --------
    raw_datasets: dict
        Dictionnary of pandas.DataFrame where keys are the files names and values are the DataFrames
    
    data: pandas.DataFrame
        DataFrame containing all merged and cleaned data.
    
    features: pandas.DataFrame
        DataFrame containing only the columns involved in classification.


    """
    def __init__(self, filenames=["customers.csv", "indicators.csv"]):
        self.filenames = filenames
        self.raw_datasets = self.get_data_from_file(filenames)
        self.predicted_dataset = None
        
    @property
    def features(self):
        return self.data[FEATURES]

    @property
    def data(self):
        return self._prepare_data()

    def get_data_from_file(self, filenames):
        #TODO : Handle cases where files are not CSV
        data_path = Path(DATA_DIR)
        paths = [data_path / name for name in filenames]
        datasets = {}
        for path in paths:
            filename = path.name
            datasets[filename] = pd.read_csv(path, delimiter=";")
        return datasets

    def _prepare_data(self):
        if len(self.raw_datasets)>1:
            df_merged = self._merge_datasets(*self.raw_datasets.values())
        else :
            df_merged = self.raw_datasets.values()[0]

        df_formated = self._format_date(df_merged)
        df_without_outliers = self._selectively_remove_nan(df_formated, REVENUE_COL)
        return df_without_outliers

    def _merge_datasets(self, df_customers, df_indicators):
        return df_customers.merge(df_indicators,on=ID_COL)
    
    def _format_date(self, df):
        df[ENTRY_DATE_COL]=pd.to_datetime(df[ENTRY_DATE_COL]) 
        return df
    
    def _selectively_remove_nan(self, df, feature):
        return df.dropna(subset = [feature])
    
    def add_predictions(self, y_pred):
        self.predicted_dataset = pd.concat([self.data.reset_index(), pd.Series(y_pred, name="PREDICTIONS")], axis=1)
    
    def export_predictions(self):
        if self.predicted_dataset is not None:
            ts = datetime.now().strftime("%Y%m%d.%H%M")
            file_path = os.path.join(PREDS_DIR, f"predictions_{ts}.csv")
            self.predicted_dataset.to_csv(file_path, index=False)
            return file_path





class TrainingDataset(Dataset):
    """
    Data structure used to train the model
    See Dataset documentation for more detail

    Properties
    --------
    All properties associated with a standard dataset

    labels: pandas.Series
        Series of labels to use for training and testing.

    """

    @property
    def labels(self):
        labels = self._format_labels(self.data[LABELS_COL])
        return labels
        
    def _format_labels(self, serie):
        """
        Replace string labels with numeric values.
        """
        return serie.replace(to_replace=["Yes", "No"], value=[1,0])

if __name__ == "__main__":
    #paths = ["customers.csv", "indicators.csv"]
    d = Dataset()
    print(d.features)
