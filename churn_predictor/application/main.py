"""
Main entry point of the classification application.
"""
from churn_predictor.infrastructure.dataset import TrainingDataset, Dataset
from churn_predictor.domain.pipeline import CHURN_PIPELINE, save_pipeline, load_pipeline
from churn_predictor.domain.dataset_splitters import RandomSplitter
from churn_predictor.settings import RESAMPLING

def predict(filepaths):
    """
    Predict if a client will Churn based on its input.
    
    Parameters
    --------
    filepath: str
        Path of the dataset to predict

    Returns
    --------
    resultpath: str
        Path of the dataset augmented with the prediction
    """
    #Collect data
    dataset = Dataset(filepaths)
    X_pred = dataset.features

    #Retrive existing model
    pipeline = load_pipeline()

    #Predict & save results
    y_pred = pipeline.predict(X_pred)
    dataset.add_predictions(y_pred)
    res_path = dataset.export_predictions()

    return res_path
    


def _train_model(filepaths):
    """
    Train the Pipeline model and save it in joblib file
    
    Parameters
    --------
    filepath: str or list<str>
        Path or list of paths of the training datasets
    """
    dataset = TrainingDataset(filepaths)
    data = RandomSplitter(dataset, resampling= RESAMPLING[0])
    pipeline = CHURN_PIPELINE.fit(*data.training_sets)
    #TODO : Replace print with logging module
    
    clf_path = save_pipeline()
    return clf_path


if __name__ == "__main__":
    paths = ["customers.csv", "indicators.csv"]
    res_path = _train_model(paths)
    print(f" --- Pipeline saved in {res_path}")
    preds = predict(paths)
    print(preds)
