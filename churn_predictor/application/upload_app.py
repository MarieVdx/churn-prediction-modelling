import os
import pandas as pd
from flask import Flask, flash, request, redirect, url_for, render_template, Response
from werkzeug.utils import secure_filename
from pathlib import Path

from churn_predictor.settings import PREDS_DIR, UPLOADS_DIR, LABELS_COL, ACCEPTED_UPLOAD_EXTENTIONS, CUSTOMERS_COLS, INDICATORS_COLS, SS_TK
from churn_predictor.application.main import _train_model, predict
from churn_predictor.domain.pipeline import existing_pipeline

UPLOAD_FOLDER = Path(UPLOADS_DIR)
ALLOWED_EXTENSIONS = {'csv'}

app = Flask(__name__)
app.secret_key = SS_TK
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['UPLOAD_EXTENSIONS'] = ACCEPTED_UPLOAD_EXTENTIONS
app.config['CUSTOMERS_HEADER'] = CUSTOMERS_COLS
app.config['INDICATORS_HEADER'] = INDICATORS_COLS
app.config["LABELS_HEADER"]=[LABELS_COL] if not isinstance(LABELS_COL, list) else LABELS_COL


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def is_valid_csv(filename, header):
    df = pd.read_csv(UPLOAD_FOLDER / filename, sep=",|;")
    test = all(column in df for column in header)
    return test

def has_labels(filename):
    df = pd.read_csv(UPLOAD_FOLDER / filename, sep=",|;")
    test = all(label_name in df.columns for label_name in app.config["LABELS_HEADER"])
    return test


def clean_upload_folder():
    for filename in os.listdir(UPLOAD_FOLDER):
        file_path = os.path.join(UPLOAD_FOLDER, filename)
        try:
            os.remove(file_path)
        except Exception as e:
            print(f"Fail to delete {file_path}")
            print(repr(e))


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        
        train_mode = False
        if request.form.get('train_mode') == 'on':
            train_mode = True       
        
        # check if the post request has the file part
        if 'customers' not in request.files or 'indicators' not in request.files:
            flash('Some csv files are missing', 'error')
            return redirect(request.url)
        
        # if user does not select file, browser also
        # submit an empty part without filename
        contains_labels = False
        paths = []
        for name in request.files:
            file = request.files[name]
            
            if file.filename == '':
                flash(f'No {name} file has been provided', 'error')
                return redirect(request.url)
            
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                file.save(file_path)

                if not is_valid_csv(filename, app.config[f"{name.upper()}_HEADER"]):
                    flash(f'Some expected columns for {name} are missing in {filename} ', 'error')
                    clean_upload_folder()
                    return redirect(request.url)
                elif train_mode and has_labels(filename):
                    contains_labels = True
                    flash(f"{name} file uploaded with success !", "success")
                else :
                    flash(f"{name} file uploaded with success !", "success")

                paths.append(file_path)

            else :
                flash(f'Incorrect type of files provided for {name}. Please use .csv', 'error')
                return redirect(request.url)
            
        if train_mode and not contains_labels:
            flash(f'Model training is not possible. No labels columns named {app.config["LABELS_HEADER"]} has been founded in the uploaded files.', 'error')
        elif train_mode :
            _train_model(paths)
            flash(f"A new model has been trained and is ready to be used.", "success")
        elif not existing_pipeline():
            flash("You can't predict values while you have trained your model once. ", "error")
            return redirect(request.url)
        else :
            res_path = predict(paths)
            return export_results(res_path)

    return render_template('index.html')


#@app.route("/export_results")
def export_results(file_path):
    with open(file_path) as fp:
        csv = fp.read()
    return Response(csv, 
        mimetype="text/csv", 
        headers={"Content-disposition":
                 "attachment; filename=predictions.csv"})
